import React, { Component, } from 'react'
import { Alert } from 'react-native'
import taskSchema from './schemas'

const Realm = require('realm');

class DB extends Component {
  
  static async get(id) {
    return Realm.open({schema: [taskSchema]})
      .then(realm => {
        return realm.objects('task').filtered('id = $0', id)[0];
    })
  }
  
  
  static async getAll() {
    return Realm.open({schema: [taskSchema]})
                    .then(realm => {
                      return realm.objects('task'); 
                    });
  }
  

  static async new(task) {  
    Realm.open({schema: [taskSchema]})
    .then(realm => {
      try {
        realm.write(() => {
          realm.create('task', {
            id  : new Date().toLocaleString(),
            title: task.title, 
            desc : task.desc,
            time : task.time == '' ? null : task.time,
            date : task.date == '' ? null : task.date
          });
        });
      } catch(e){
        Alert.alert('Error on creation')
      }
    });
  }
  
  static async delete(id) {
    Realm.open({schema: [taskSchema]})
      .then(realm => {
        try {
          realm.write(() => {
            let task = realm.objects('task').filtered('id = $0', id);
            realm.delete(task);
          }) 
        } catch(e) {
          Alert.alert('Error deleting task')
        }
    })
  }
  
  static async update(task) {
        Realm.open({schema: [taskSchema]})
      .then(realm => {
        try {
          realm.write(() => {
            realm.create('task', {
              id  : task.id,
              title: task.title, 
              desc : task.desc,
              time : task.time == '' ? null : task.time,
              date : task.date == '' ? null : task.date
            }, true);
        });
        } catch(e) {
          // TODO get rid of alert before shipping
          Alert.alert('Error updating task')
        }
    })
  }

}

export default DB