const taskSchema = {
  name: 'task',
  primaryKey: 'id',
  properties: {
    id: 'string',
    title: 'string',
    desc: 'string?', //optional
    time: 'date?', //optional
    date: 'date?' //optional
  }
};

export default taskSchema