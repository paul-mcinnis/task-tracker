import React, { Component, } from 'react'
import { Alert, Text, TouchableOpacity, View, } from 'react-native'
import moment from 'moment';
import { NavigationActions } from 'react-navigation'
import DB from '../db/DB'
import styles from './styles'

const resetAction = NavigationActions.reset({
  index: 0,
  actions: [
    NavigationActions.navigate({ routeName: 'Home'})
  ]
})

class TaskInfo extends Component {

  constructor(props) {
    super(props);
    this.state = {
      id: '',
      title: 'placeholder',
      desc: 'placeholder',
      time: '',
      date: '',
    };
  }
  
  _deleteTask = () => {
    DB.delete(this.state.id)
    Alert.alert( 'Task Deleted','', [{text: 'OK', onPress: () => this.props.navigation.dispatch(resetAction)}])
  }
  
  componentDidMount(){
    DB.get(this.props.navigation.state.params.id)
      .then(res => {
        this.setState({
          id: res.id,
          title: res.title,
          desc: res.desc,
          time: res.time == null ? '' : moment(res.time).format('hh:mm a'),
          date: res.date == null ? '' : moment(res.date).format('ddd Do MMM')
        });
    })
  }
  
  
  render() {
    const { goBack, navigate } = this.props.navigation
    return (
      <View style={styles.main}>
        
        <View style={styles.titleContainer}>
          <Text style={styles.title}> Task Info </Text>
        </View>
        
        <View style={styles.bodyContainer}>
          
          <Text style={styles.label}> Task </Text>
          <Text style={styles.text}> {this.state.title} </Text>
          
          <Text style={styles.label}> Description </Text>
          <Text style={styles.text}> {this.state.desc == '' ? 'No Description' : this.state.desc} </Text>
          
          <Text style={styles.label}> Time </Text>
          <Text style={[styles.text, styles.dateTime]}> {this.state.time == '' ? 'No Time' : this.state.time} </Text>
          
          <Text style={styles.label}> Date </Text>
          <Text style={[styles.text, styles.dateTime]}> {this.state.date == '' ? 'No Date' : this.state.date} </Text>
        </View>
        
        <View style={styles.controlsContainer}>
          
          <TouchableOpacity
            style={styles.buttonContainer}
            onPress={() => goBack(null)}  
            accessibilityLabel="Cancel">
            <Text style={[styles.button, styles.cancel]}> Cancel </Text>
          </TouchableOpacity>
          
          <TouchableOpacity
            style={styles.buttonContainer}
            onPress={() => this._deleteTask()}   
            accessibilityLabel="Delete Task">
            <Text style={[styles.button, styles.delete]}> Delete </Text>
          </TouchableOpacity>

          <TouchableOpacity
            style={styles.buttonContainer}
            onPress={() => navigate('EditTask', {id: this.state.id})} 
            accessibilityLabel="Delete Task">
            <Text style={[styles.button, styles.edit]}> Edit </Text>
          </TouchableOpacity>
          
        </View>
      </View>
    )
  }
}

export default TaskInfo