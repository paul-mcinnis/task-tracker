import Home from './home'
import NewTask from './newTask'
import EditTask from './editTask'
import TaskInfo from './taskInfo'

export {Home};
export {NewTask};
export {EditTask};
export {TaskInfo};