import React, { Component, } from 'react'
import { View } from 'react-native'
import { NewTaskButton, TaskList} from '../components/index'
import styles from './styles'


class Home extends Component {
  
  render() {
    return (
      <View style={styles.main}>
        <TaskList navigation={this.props.navigation}/>
        <NewTaskButton navigation={this.props.navigation}/>
      </View>
    );
  }
}


export default Home