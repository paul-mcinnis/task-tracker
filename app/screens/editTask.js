import React, { Component, } from 'react'
import { 
  Alert, 
  Button, 
  View, 
  Text, 
  TextInput,
  TouchableOpacity
} from 'react-native'
import DateTimePicker from 'react-native-modal-datetime-picker'
import Icon from 'react-native-vector-icons/FontAwesome'
import moment from 'moment'
import { NavigationActions } from 'react-navigation'
import DB from '../db/DB'
import styles from './styles'

const resetAction = NavigationActions.reset({
  index: 0,
  actions: [
    NavigationActions.navigate({ routeName: 'Home'})
  ]
})

class EditTask extends Component {

  constructor(props) {
    super(props);
    this.state = {
      id: '',
      title: '',
      desc: '',
      time: null,
      date: null,
      isTimePickerVisible: false,
      isDatePickerVisible: false,
      hasChanged: false
    };
  }
  
    componentDidMount(){
      DB.get(this.props.navigation.state.params.id)
        .then(res => {
          this.setState({
            id: res.id,
            title: res.title,
            desc: res.desc,
            time: res.time,
            date: res.date}
          );
      })
  }
  
  _showDatePicker = () => this.setState({ isDatePickerVisible: true });
  _showTimePicker = () => this.setState({ isTimePickerVisible: true });

  _hideDatePicker = () => this.setState({ isDatePickerVisible: false });
  _hideTimePicker = () => this.setState({ isTimePickerVisible: false });

  _handleDatePicked = (DATE) => {
    var date = new Date(moment(DATE).format('MMM, D, YYYY')) //reformats date to remove hours minutes, etc, important for sorting
    this.setState({date: date, hasChanged: true})
    this._hideDatePicker();
  };
  
  _handleTimePicked = (TIME) => {
    var time = new Date(TIME.setFullYear(1970, 0, 1))
    this.setState({time: time, hasChanged: true})
    this._hideTimePicker();
  };
  
  _saveTask = (goBack) => {
    var task = {
      id: this.state.id,
      title: this.state.title,
      desc: this.state.desc,
      time: this.state.time,
      date: this.state.date == '' && this.state.time != '' ? new Date() : this.state.date
    }
      DB.update(task);
      Alert.alert( 'Success','', [{text: 'OK', onPress: () => this.props.navigation.dispatch(resetAction)}])
  };
  
  render() {
    const { goBack } = this.props.navigation;
    var noon = new Date(1970, 1, 1, 12, 0);
    return (
      <View style={styles.main}>
        
        <View style={styles.titleContainer}>
          <Text style={styles.title}> Edit Task </Text>
        </View>
        
        <View style={styles.bodyContainer}>
          <TextInput
            style={styles.textInput}
            maxLength= {25}
            value={this.state.title}
            placeholder= "Task (Required)"
            placeholderTextColor= "black"
            onChangeText={(title) => {
              this.setState({title, hasChanged: true})
              }
            }
          />
          <TextInput
            style={[styles.textInput, styles.description]}
            value={this.state.desc}
            placeholder= "Description"
            placeholderTextColor= "black"
            multiline= {true}
            onChangeText={(desc) => this.setState({desc, hasChanged: true})}
          />
          
          <Text style={styles.label}> Time (Optional)</Text>
          <TouchableOpacity
            onPress={this._showTimePicker}>
            <View style={{alignItems: 'center'}}>
              <Icon style={styles.calendarIcon} name="clock-o" size={40} color='blue' />
              <Text style={styles.dateTime}> 
                {this.state.time == null ? '-- : --' : moment(this.state.time).format('hh : mm a')} 
              </Text>
            </View>
          </TouchableOpacity>
          <DateTimePicker
            mode= "time"
            date= {noon}
            isVisible={this.state.isTimePickerVisible}
            onConfirm={this._handleTimePicked}
            onCancel={this._hideTimePicker}
          />
          
          <Text style={styles.label}> Date (Optional) </Text>
          <TouchableOpacity onPress={this._showDatePicker}>
            <View style={{alignItems: 'center'}}>
              <Icon style={styles.calendarIcon} name="calendar" size={40} color='blue' />
              <Text style={styles.dateTime}> 
                {this.state.date == null ? 'DD/MM/YYYY' : moment(this.state.date).format('DD/MM/YYYY')}
              </Text>
            </View>
          </TouchableOpacity>
          <DateTimePicker
            mode= "date"
            isVisible={this.state.isDatePickerVisible}
            onConfirm={this._handleDatePicked}
            onCancel={this._hideDatePicker}
          />
        </View>
        
        <View style={styles.controlsContainer}>
          <TouchableOpacity
            style={styles.buttonContainer}
            onPress={() => goBack(null)}  
            accessibilityLabel="Cancel">
            <Text style={[styles.button, styles.cancel]}> Cancel </Text>
          </TouchableOpacity>
          
          <TouchableOpacity
            style={styles.buttonContainer}
            onPress={() => this._saveTask(goBack)}
            disabled={this.state.title == ''}
            accessibilityLabel="Save">
            <Text style={[styles.button, this.state.title == '' ? styles.disabled : styles.save]}> Save </Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}

export default EditTask