import { Dimensions, PixelRatio, Platform, StyleSheet } from 'react-native';

const screenW = Dimensions.get('window').width;
const screenH = Dimensions.get('window').height;

const styles = StyleSheet.create({
  background: {
    height: screenH,
    width: screenW,
  },
  bodyContainer: {
    height: screenH * 0.8,
    alignItems: 'center',
  },
  button: {
    fontFamily: 'Arial',
    fontSize: 22,
  },
  buttonContainer: {
    height: screenH * 0.1,
    width: screenW * 0.7 / 3,
    marginLeft: screenW * 0.05,
    marginRight: screenW * 0.05,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    
  },
  cancel: {
    color: 'red',
  },
  controlsContainer: {
    flexDirection: 'row', 
    justifyContent: 'space-between',
  },
  calendarIcon: {
    color: 'blue',
    fontSize: 50,
    margin: 10
  },
  dateTime: {
    fontWeight: 'bold', 
    color: 'blue'
  },
  delete: {
    color: 'darkred',
  },
  description: {
    height: screenH * 0.2,
  },
  descriptionContainer: {
    height: screenH * 0.25,
    alignItems: 'center',
  },
  disabled: {
    color: 'grey',
  },
  edit: {
    color: '#841584',
  },
  heading: {
    marginBottom: 15,
    fontSize: 18,
    fontWeight: 'bold'
  },
  label: {
    fontWeight: 'bold',
    fontSize: 22,
    marginBottom: 5,
  },
  main: {
    flex: 1, 
    flexDirection: 'column',
    backgroundColor: 'white',
    paddingTop: Platform.OS === 'ios' ? 25: 0,
    paddingBottom: Platform.OS === 'android' ? 25: 0,
  },
  pickerContainer: {
    height: screenH * 0.175,
    paddingTop: screenH * 0.05,
    alignItems: 'center',
  },
  save: {
    color: 'green',
  },
  taskContainer: {
    height: screenH * 0.1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  taskListContainer: {
    flex:1,
    alignItems: 'center',
    marginTop: screenH * 0.05,
    backgroundColor: 'transparent'
  },
  text: {
    fontSize: 20,
    marginTop: 5,
    marginBottom: 15,
  },
  textInput: {
    height: 40,
    margin: 5,
    padding: 5,
    borderWidth: 2,
    borderRadius: 5,
    width: screenW * 0.9,
    borderColor: 'grey'
  },
  title: {
    fontSize: 20,
    fontFamily: "Arial",
    fontWeight: 'bold',
  },
  titleContainer: {
    height: Platform.OS === 'ios' ? screenH * 0.1 : screenH * 0.05,
    alignItems: 'center',
  }
});

export default styles