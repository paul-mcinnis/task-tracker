'use strict';

import React from 'react';
import { EditTask, Home, NewTask, TaskInfo} from './screens/index';
import { StackNavigator, } from 'react-navigation';

const App = StackNavigator({
  
  Home: {
    screen: Home,
    navigationOptions: {
      header: null,
   },
  },
  EditTask: {
    screen: EditTask,
    navigationOptions: {
      header: null,
    },
  },
  NewTask: {
    screen: NewTask,
    navigationOptions: {
      header: null,
    },
  },
  TaskInfo: {
    screen: TaskInfo,
    navigationOptions: {
      header: null,
    }
  }
});

export default App;