import { Dimensions, StyleSheet } from 'react-native';

const screenW = Dimensions.get('window').width;
const screenH = Dimensions.get('window').height;

const styles = StyleSheet.create({
  icon: {
    
  },
  iconContainer: {
    position: 'absolute',
    marginLeft: screenW * 0.8,
    marginTop: screenH * 0.85,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'darkred',
    height:60,
    width: 60,
    borderRadius: 30,
    borderWidth: 2
  }
});

export default styles;