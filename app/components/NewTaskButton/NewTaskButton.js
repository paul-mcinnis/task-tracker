import React, { Component, } from 'react'
import { 
  Alert,
  TouchableHighlight, 
  View 
} from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome';
import styles from './styles'
  
class NewTaskButton extends Component {

  render() {
    const { navigate } = this.props.navigation
    return (
      <TouchableHighlight 
        onPress={() => navigate('NewTask')}
        style={styles.iconContainer}>
        <Icon style={styles.icon} name="pencil" size={30} color='white' />
      </TouchableHighlight>
    );
  }
}

export default NewTaskButton