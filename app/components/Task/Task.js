import React, { Component, } from 'react'
import { 
  Alert, 
  Text, 
  TouchableHighlight,
  TouchableWithoutFeedback, 
  View 
} from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome';
import moment from 'moment';
import { NavigationActions } from 'react-navigation'
import Swipeout from 'react-native-swipeout'
import DB from '../../db/DB'
import styles from './styles'

const resetAction = NavigationActions.reset({
  index: 0,
  actions: [
    NavigationActions.navigate({ routeName: 'Home'})
  ]
})

class Task extends Component {
  
  render(){

    return(
      <View>
        <Body
          navigation={this.props.navigation} 
          id={this.props.id} 
          title={this.props.title} 
          desc={this.props.desc} 
          time={this.props.time} 
          date={this.props.date}
          />
      </View>
    );
  }
}

class Body extends Component {
  
    _deleteTask = () => {
    DB.delete(this.props.id)
    Alert.alert( 'Task Deleted','', [{text: 'OK', onPress: () => this.props.navigation.dispatch(resetAction)}])
  }
  
  render(){
    var date = this.props.date == null ? '' : moment(this.props.date).format('ddd Do MMM');
    var time = this.props.time == null ? '' : moment(this.props.time).format('hh:mm a');
    const { navigate } = this.props.navigation
    return(
      <View> 
      <View style={styles.container}>
        <Swipeout
          style={styles.swipeout}
          autoClose= {true}
          right={[{
            text: 'edit',
            type: 'primary',
            color: '#841584',
            backgroundColor: 'white',
            onPress: () => navigate('EditTask', {id: this.props.id})
          },
          {
            text: 'delete',
            type: 'secondary',
            color: 'darkred',
            backgroundColor: 'white',
            onPress: () => this._deleteTask()
          }]}
          >
          <TouchableWithoutFeedback
            onPress={() => navigate('TaskInfo',{
              id: this.props.id,
              })}>
            <View style={styles.buttonContainer}>
              <View>
                <Text style={styles.title}>{this.props.title}</Text>
                <View style={{flexDirection: 'row'}}>
                  <Text style={styles.date}> {time}</Text>
                  <Text style={styles.date}>{date}</Text>
                </View>
              </View>
              <View style={styles.iconContainer}>
                <Icon style={styles.icon} name="arrow-left" size={30} color='#162129' />
              </View>
            </View>
          </TouchableWithoutFeedback>
        </Swipeout>
      </View>
      </View>
    );
  }
}

export default Task