import React, { Component, } from 'react'
import { 
  FlatList, 
  Text,
  View 
} from 'react-native'
import { NoTasks, Task } from '../index'
import DB from '../../db/DB'
import Sort from './sort'
import styles from './styles'

class TaskList extends Component {
  
  constructor(props) {
    super(props);
    this.state = {
      data: '',
      isLoading: true
    };
  }
  
  componentDidMount(){
    DB.getAll()
    .then(res => {
      var sortedData = Sort.sort(res)
      var categorizedData = Sort.categorize(sortedData)
      this.setState({data: categorizedData, isLoading: false})
    })
  }

  render(){
    if (this.state.isLoading == true){
      return (
        <View style={styles.container}>
          <Text> . . . Loading </Text>
        </View>
      );
    }
    else if (this.state.data == '') {
      return (
        <View style={styles.container}>
          <NoTasks></NoTasks>
        </View>
      );
    } else {
      return (
        <View style={styles.container}>
          <FlatList
            data= {this.state.data}
            keyExtractor={item => item.id}
            renderItem={({item}) => 
              <View>
                <Header category={item.category}></Header>
                <Task 
                  navigation={this.props.navigation}
                  id={item.id} 
                  title={item.title} 
                  desc={item.desc} 
                  time={item.time} 
                  date={item.date}/>
              </View>
            }
          />
        </View>
      );
    }
  }
}

class Header extends Component {
  
  render(){
    if (this.props.category != ''){
      return(
        <View>
          <Text style={styles.header}>{this.props.category}</Text>
        </View>
      );
    } else {
      return null
    }
  }
}

export default TaskList