import { Dimensions, StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex:1,
    alignItems: 'center',
    backgroundColor: 'transparent'
  },
  header: {
    fontSize: 22,
    fontWeight: 'bold',
    marginBottom: 5,
  }
});

export default styles