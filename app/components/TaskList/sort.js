import React, { Component, } from 'react'
import { Alert } from 'react-native'

class Sort extends Component {
  
    /* The first element in each category is assigned a category to be displayed by TaskList, 
   * Categories :
   * General: date == null
   * Passed: date < midnight yesterday
   * Today:  midnight yesterday <= date < midnight today
   * Tomorrow: midnight today <= date < midnight tomorrow
   * This Week: midnight tomorrow <= date < midnight next Sunday *NOTE will not display if tomorrow is Sunday
   * Next Week: midnight next Sunday <= date < midnight next Sunday + 7 days
   * The Future: midnight next sunday + 7 days <= date
   */
  
  static categorize(sortedData){
    var arr = [];
    var currCat = ''
    
    var yesterday = new Date()
    yesterday.setHours(0,0,0,0);
    var today = new Date();
    today.setHours(24,0,0,0);
    var tomorrow = new Date(today)
    tomorrow.setDate(today.getDate() + 1)
    var thisSunday = new Date(today)
    thisSunday.setDate(yesterday.getDate() + 7 -  yesterday.getDay())
    var nextSunday = new Date(today)
    nextSunday.setDate(thisSunday.getDate() + 7)
    
    for (var i = 0; i < sortedData.length; i++){
      var elem = sortedData[i];
      if (elem.date != null){
        var date = elem.date.getTime()
      } else {
        var date = null
      }
      
      if (date == null){
        if(currCat != 'General'){
          elem.category = 'General'
          currCat = 'General'
        } else {
          elem.category = ''
        }
      } else if (date < yesterday.getTime()){
        if(currCat != 'Expired'){
          elem.category = 'Expired'
          currCat = 'Expired'
        } else {
          elem.category = ''
        }
      } else if (date < today.getTime()){
        if(currCat != 'Today'){
          elem.category = 'Today'
          currCat = 'Today'
        } else {
          elem.category = ''
        }
      } else if (date < tomorrow.getTime()){
        if(currCat != 'Tomorrow'){
          elem.category = 'Tomorrow'
          currCat = 'Tomorrow'
        } else {
          elem.category = ''
        }
      } else if (date < thisSunday.getTime()){
        if(currCat != 'This Week'){
          elem.category = 'This Week'
          currCat = 'This Week'
        } else {
          elem.category = ''
        }
      } else if (date < nextSunday.getTime()){
        if(currCat != 'Next Week'){
          elem.category = 'Next Week'
          currCat = 'Next Week'
        } else {
          elem.category = ''
        }
      } else if (currCat != '2+ Weeks'){
        elem.category = '2+ Weeks'
        currCat = '2+ Weeks'
      } else {
        elem.category = ''
      }
      arr.push(elem);
    }
    return arr
  }
  
  
  // biased towards tasks added later in the event of a tie
   static _isMoreRecent = (task1, task2) => {
     if (task1.date == null){
       return true
     } else if (task2.date == null){
       return false
     } else if (task1.date.getTime() < task2.date.getTime()){
       return true
     } else if (task1.date.getTime() == task2.date.getTime()){
       if (task1.time == null){
         return true;
       } else if (task2.time == null){
         return false;
       } else {
         return (task1.time.getTime() < task2.time.getTime())
       }
     } else {
       return false
     }
  }
  
  // Insertion sorting algorythm to rearange tasks into chronological order
  static sort(tasks){
    var arr = [];
    var length = tasks.length
    for (var i = 0; i < length; i++){
      arr.push(tasks[i])
    }
    for (var i = 0; i < length; i++){
      var j = i;
      while (j > 0 && this._isMoreRecent(arr[j], arr[j-1])){
        var temp = arr[j];
        arr[j] = arr[j-1];
        arr[j-1] = temp;
        j -= 1;
      }
    }
    return arr
  }
  
  static 
  
} 

export default Sort