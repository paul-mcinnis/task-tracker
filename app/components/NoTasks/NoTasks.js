import React, { Component, } from 'react'
import { Image, Text, View } from 'react-native'
import styles from './styles'

class NoTasks extends Component {
  
  render(){
      return (
        <View style={styles.container}>
          <Image style={styles.image} source={require('../../assets/noTasks.png')} />
        </View>
      );
  }
}

export default NoTasks