import { Dimensions, StyleSheet } from 'react-native';

const screenW = Dimensions.get('window').width;

const styles = StyleSheet.create({
  container: {
    flex:1,
    alignItems: 'center',
  },
  image: {
    marginTop: 15,
    marginLeft: screenW * 0.1,
    marginRight: screenW * 0.1,
  }
});

export default styles