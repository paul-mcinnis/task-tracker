# Welcome to Task Tracker

#### Task Traker is a simple mobile Application built using the [React Native](https://facebook.github.io/react-native/) Engine.

Task Traker was originally built for personally use. Meaning that it is not yet available on the Google Play Store nor the App App store. If you wish to load this on your personnal device you'll have to set your phone to dev mode and upload it via xCode for iPhones or Android Studios for Android.

As I (we) get closer to deployment or if more people wish to contribute I'll update this readme and actually fill out the contribution guide. Otherwise feel free to [contact me](mcinnis.paul@gmail.com) if you have any questions.